# created with the help of this link:
# https://www.desgehtfei.net/en/quick-start-kvm-libvirt-vms-with-terraform-and-ansible-part-1-2/

# Kubernetes setup:
# https://github.com/MusicDin/terraform-kvm-kubespray

# Use terraform.tfvars to define the settings of your servers
# the variables here are the defaults if no terraform.tfvars setting is found
variable "projectname" {
 type   = string
 default = "myproject"
}
variable "hosts" {
  default = {
    # "srv1" = {
    #    name = "srv1",
    #    vcpu     = 1,
    #    memory   = "1024",
    #    diskpool = "default",
    #    disksize = "10737418240", # 10 GiB
    #  },
  }
}
variable "baseimagediskpool" {
  type    = string
  default = "default"
}
variable "domainname" {
  type    = string
  default = "domain.local"
}
variable "networkname" {
  type    = string
  default = "default"
}
variable "sourceimage" {
  type    = string
  default = "https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2"
}

# Base OS image
resource "libvirt_volume" "centos-8-base" {
  name = "centos-8-base_${var.projectname}"
  source = var.sourceimage
  pool   = var.baseimagediskpool
}

# Create a virtual disk per host based on the Base OS Image
resource "libvirt_volume" "qcow2_volume" {
  for_each = var.hosts
  name           = "${var.projectname}_${each.value.name}.qcow2"
  base_volume_id = libvirt_volume.centos-8-base.id
  pool           = each.value.diskpool
  format         = "qcow2"
  size           = each.value.disksize
}

# Use cloudinit config file and forward some variables to cloud_init.cfg
data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.yml")
  for_each   = var.hosts
  vars     = {
    hostname   = each.value.name
    domainname = var.domainname
  }
}

# Use CloudInit to add the instance
resource "libvirt_cloudinit_disk" "commoninit" {
  for_each   = var.hosts
  name      = "commoninit_${each.value.name}.iso"
  user_data = data.template_file.user_data[each.key].rendered
  # network_config = data.template_file.network_config.rendered
}


# Define KVM-Guest/Domain
resource "libvirt_domain" "cloudstack-hosts" {
  for_each   = var.hosts
  name   = "${var.projectname}_${each.value.name}"
  memory = each.value.memory
  vcpu   = each.value.vcpu

  cpu {
    mode = "host-passthrough"
  }

  network_interface {
    network_name   = var.networkname
    # If networkname is host-bridge do not wait for a lease
    wait_for_lease = var.networkname == "host-bridge" ? false : true
  }

  disk {
    volume_id = element(libvirt_volume.qcow2_volume[each.key].*.id, 1 )
  }

  cloudinit = libvirt_cloudinit_disk.commoninit[each.key].id

}
## END OF KVM DOMAIN CONFIG

# Output results to console
output "hostnames" {
  value = [libvirt_domain.cloudstack-hosts.*]
}
