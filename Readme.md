# Automated single server Cloudstack installation

This are configuration files to setup a virtual machine on KVM and install a single server Cloudstack instance via Ansible.
At the moment this installation is based on the [Cloudstack Quickstart guide](https://docs.cloudstack.apache.org/en/latest/quickinstallationguide/qig.html).


## Terraform

Terraform is used to create the virtual machine on the kvm host.

### Usage

Configure the `terraform.tfvars` file to your liking.
Then init and deploy:  

`cd terraform`

`terraform init`

`terraform deploy`


## Ansible

Ansible is used to install and configure the server to be a single instance Cloudstack server.

### Usage

Update the `inventory.yml` to include your host.

Then configure it with ansible: `ansible-playbook -i inventory.yml site.yml --skip-tags "cloudstack-configuration" `

Now login under `http://http://<server-ip>:8080/` go to `Accounts`, `admin`, `View Users`, `admin` and click onto the `Generate keys` icon in the top right corner. Confirm the prompt with `OK`.

Update the `vars.env` file with the new generated API credentials.

Now configure the rest of Cloudstack by running the configuration part:
`env $(cat vars.env | xargs)  ansible-playbook -i inventory.yml site.yml --tags "cloudstack-configuration"`

## Roadmap

- [x] configure Cloudstack and add the host
- [ ] adapt installation to full install guide
- [ ] secure mysql installation
- [ ] configure the firewall
- [ ] split configuration from code
- [ ] create missing selinux rules and change selinux to enforcing